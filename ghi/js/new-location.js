/** This is a description of the window.addEventListener function.

    The code needed when the page loads will call that RESTful API
    you just created, get the data back, then loop through it.
    And for each state in it, it'll create an option element that
    has a value of the abbreviation and the text of the name.

    Pseudocode:
        Get the select tag element by its id 'state'
        https://developer.mozilla.org/en-US/docs/Web/API/Document/getElementById

        For each state in the states property of the data

            Create an 'option' element
            // https://developer.mozilla.org/en-US/docs/Web/API/Document/createElement

            Set the '.value' property of the option element to the
            state's abbreviation

            Set the '.innerHTML' property of the option element to
            the state's name

            Append the option element as a child of the select tag

*/
window.addEventListener("DOMContentLoaded", async () => {

    const url = "http://localhost:8000/api/states/";

    const response = await fetch(url);

    // Displays list of states in the Create Location drop down menu
    if (!response.ok) {
        // TODO: Figure out what to do when the response is bad
    } else {
        const data = await response.json();
        console.log("~~~~ data", data);

        const selectTag = document.getElementById("state");

        for (let state of data.states) {
            const optionTag = document.createElement("option");
            optionTag.value = state.abbreviation;

            optionTag.innerHTML = state.name;

            selectTag.appendChild(optionTag);
        }
    }
    // Handles the HTMLFormElement submit event: https://shorturl.at/flszD
    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async event => {
    event.preventDefault();

    const formData = new FormData(formTag);
    const json = JSON.stringify(Object.fromEntries(formData));
    const locationUrl = 'http://localhost:8000/api/locations/';
    const fetchConfig = {
        method: "post",
        body: json,
        headers: {
            'Content-Type': 'application/json',
        },
    }
    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
        formTag.reset();
        const newLocation = await response.json();
        console.log(newLocation);
    }
  });
});
