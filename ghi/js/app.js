/** This is a description of the createCard function.

    • .card shadow: adds shadow to the cards
      • https://getbootstrap.com/docs/5.3/utilities/shadows/#examples

    • .card-subtitle: pulls the name of the location to the card
      • https://getbootstrap.com/docs/5.1/components/card/#titles-text-and-links
      • mb stands for margin border; text-muted makes the font gray.

    • .card-footer: pulls the start and end dates
      • https://getbootstrap.com/docs/5.1/components/card/#header-and-footer
      • https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/toLocaleDateString

*/
function createCard({name, description, pictureUrl, starts, ends, location}) {
  return `
        <div class="card shadow p-3 mb-5 bg-body-tertiary rounded">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                <p class="card-text">${description}</p>
            </div>
            <h5 class="card-footer">${new Date(starts).toLocaleDateString()} - ${new Date(ends).toLocaleDateString()}</h5>
        </div>
    `;
}

window.addEventListener("DOMContentLoaded", async () => {
  const url = "http://localhost:8000/api/conferences/";

  try {
    const response = await fetch(url);

    if (!response.ok) {
      // https://getbootstrap.com/docs/5.1/components/alerts/
      const errorMessage = "Failed to fetch conference data";
      const alertDiv = document.querySelector(".alert");
      alertDiv.innerHTML = `<div class="alert alert-danger" role="alert">${errorMessage}</div>`;
      // https://getbootstrap.com/docs/4.1/utilities/visibility/
      alertDiv.classList.remove('invisible');

    } else {
      const data = await response.json();

      let i = 0;

      for (let conference of data.conferences) {
        // Gets the data from APIs
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          // Gets the name of the conference
          const name = details.conference.name;
          // Gets the description of the conference
          const description = details.conference.description;
          // Gets the photo of the location from Pexels API
          const pictureUrl = details.conference.location.picture_url;
          // Creates footer containing the start and end dates for each conference
          const starts = details.conference.starts;
          const ends = details.conference.ends;
          // Creates a subtitle containing the name of each conference's location
          const location = details.conference.location.name;
          // Creates cards using the createCard function above and the information
          const html = createCard({name, description, pictureUrl, starts, ends, location});
          // Gets the newly created cards in to document, specifically the columns
          const column = document.querySelector(`#col-${i % 3}`);
          column.innerHTML += html;
          i++;
        }
      }
    }
  } catch (error) {
    // https://getbootstrap.com/docs/5.1/components/alerts/
    const errorMessage = "An error occurred while fetching conference data";
    const alertDiv = document.querySelector(".alert");
    alertDiv.innerHTML = `<div class="alert alert-danger" role="alert">${errorMessage}</div>`;
    // https://getbootstrap.com/docs/4.1/utilities/visibility/
    alertDiv.classList.remove('invisible');
  }
});
