import React, { useEffect, useState } from "react";

export default function LocationForm(props) {
  // State Selection Data
  const [states, setStates] = useState([]);

  // Form Data
  const [name, setName] = useState("");
  const [roomCount, setRoomCount] = useState("");
  const [city, setCity] = useState("");
  const [state, setState] = useState("");

  // Handlers: onChange (helper functions)
  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  };

  const handleRoomCountChange = (event) => {
    const value = event.target.value;
    setRoomCount(value);
  };

  const handleCityChange = (event) => {
    const value = event.target.value;
    setCity(value);
  };

  const handleStateChange = async (event) => {
    const value = event.target.value;
    setState(value);
  };

  //   console.log("~~~~ value", value);

  // Handler: onSubmit (helper function)
  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.room_count = roomCount;
    data.name = name;
    data.city = city;
    data.state = state;
    console.log("~~~~ data", data);

    const locationUrl = "http://localhost:8000/api/locations/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      const newLocation = await response.json();
      console.log("~~~~ newLocation", newLocation);

      setName("");
      setRoomCount("");
      setCity("");
      setState("");
    }
  };

  // Fetch Data (helper function)
  const fetchData = async () => {
    const url = "http://localhost:8000/api/states/";

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setStates(data.states);
    }
  };

  // useEffects do something at a given time
  useEffect(
    // The something is the first parameter, an anonymous function
    () => {
      fetchData();
    },
    // The given time is defined by the dependency array.
    // An empty array means "do it only once, at mounting (beginning)"
    []
  );

  // JSX
  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new location</h1>
          <form onSubmit={handleSubmit} id="create-location-form">
            <div className="form-floating mb-3">
              <input
                value={name}
                onChange={handleNameChange}
                placeholder="Name"
                required
                type="text"
                name="name"
                id="name"
                className="form-control"
              />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                key={state.abbreviation}
                value={roomCount}
                onChange={handleRoomCountChange}
                placeholder="Room count"
                required
                type="number"
                name="room_count"
                id="room_count"
                className="form-control"
              />
              <label htmlFor="room_count">Room count</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={city}
                onChange={handleCityChange}
                placeholder="City"
                required
                type="text"
                name="city"
                id="city"
                className="form-control"
              />
              <label htmlFor="city">City</label>
            </div>
            <div className="mb-3">
              <select
                value={state}
                onChange={handleStateChange}
                required
                name="state"
                id="state"
                className="form-select"
              >
                <option value="">Choose a state</option>
                {states.map((state) => {
                  return (
                    <option key={state.abbreviation} value={state.abbreviation}>
                      {state.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}
